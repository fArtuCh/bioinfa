import matplotlib.pyplot as plt
import numpy as np
import Markov as M
from decimal import Decimal
import random

random.seed(1)
def viterbi_algorithm(observation, states, switch_state, emit_p):
    """
    :param observation:
    :param states:
    :param switch_state:
    :param emit_p:
    :return: int Array - zwraca jaki stan wyśtepował w momencie generowania elementu
    """
    V = []
    n = len(observation)
    V.append([Decimal(1)] + [Decimal(0)] * (len(states) - 1))
    V_arg = [[]]
    for i in range(1, n):
        V.append([])
        V_arg.append([])
        for st in states:
            max_prob = V[i - 1][states[0]] * Decimal(switch_state[states[0]][st])
            prev_st_selected = states[0]
            for prev_st in states[1:]:
                tr_prob = V[i - 1][prev_st] * Decimal(switch_state[prev_st][st])
                if tr_prob > max_prob:
                    max_prob = tr_prob
                    prev_st_selected = prev_st

            max_prob = max_prob * Decimal(emit_p[st][int(observation[i]) - 1])
            V[i].append(max_prob)
            V_arg[i].append(prev_st_selected)

    opt = []
    max_prob = 0.0
    # Get most probable state and its backtrack
    for st, data in zip(V_arg[-1], V[-1]):
        if data > max_prob:
            max_prob = data
            best_st = st
    opt.append(best_st)
    previous = best_st

    for t in range(len(V) - 2, -1, -1):
        opt.insert(0, V_arg[t + 1][previous])
        previous = V_arg[t + 1][previous]
    return opt

def Prefiks(observation, states, ast, emit_p):
    # ast - prawdop ,przejscia ze stanu  S do stanu t,    Akl , przejscie ze stanu k do l
    #emit-p = prawdopodobienstwo emisji   Ek(b) -stan k i symbol b
    # inicjalizacja
    # pi - sekwencja stanow
    # Fk(i) - P(x1,...xi,pi=k)
    # Fl(i+1) = el(xi+1) EkFk(i)akl
    F =[]
    F.append([Decimal(1)] + [Decimal(0)] * (len(states) - 1))
    L = len(observation)

    for i in range(1, L):
        F.append([])

        for st in states:
            tr_prob = F[i - 1][states[0]] * Decimal(ast[states[0]][st])
            for prev_st in states[1:]:
                tr_prob += F[i - 1][prev_st] * Decimal(ast[prev_st][st])

            F[i].append(tr_prob * Decimal(emit_p[st][int(observation[i]) - 1]) )

    wynik = 0
    for st in states:
        wynik += F[st][-1]
    return wynik, F

def sufiks(observation, states, ast, emit_p):

    L = len(observation)
    B = [[] for x in range(L-1) ]
    B.append([Decimal(1)] * (len(states)))

    for i in reversed(range(L -1)):
        for st in states:
            tr_prob = B[i + 1][states[0]] * Decimal(ast[states[0]][st])
            for prev_st in states[1:]:
                tr_prob += B[i + 1][prev_st] * Decimal(ast[prev_st][st])

            B[i].append(tr_prob * Decimal(emit_p[st][int(observation[i]) - 1]))
    return B

def aposteriori(Pr, F, B, states=[0, 1]):
    post = []
    for i, (f, b) in enumerate(zip(F, B)):
        post.append([])
        for st in states:
            post[i].append(Decimal(f[st]) * Decimal(b[st]) / Decimal(Pr))

    return post

Kasyno = M.Gra()

Prob_switch_state = {"Uczciwa": {"Uczciwa": 0.95, "Falszywa": 0.05}, "Falszywa": {"Uczciwa": 0.1, "Falszywa": 0.9}}
Switch_state = [[0.95, 0.05], [0.1, 0.9]]

Kosc1 = {"1": 1/6, "2": 1/6, "3": 1/6, "4": 1/6, "5": 1/6, "6": 1/6, }
Kosc2 = {"1": 0.1, "2": 0.1, "3": 0.1, "4": 0.1, "5": 0.1, "6": 0.5, }

Emit_p = [[1/6, 1/6, 1/6, 1/6, 1/6, 1/6,], [0.1, 0.1, 0.1, 0.1, 0.1, 0.5]]

Kasyno.set_switch_prob_for_all_states(Prob_switch_state)
Kasyno.set_dict_transition_for_state(Kosc1, "Uczciwa")
Kasyno.set_dict_transition_for_state(Kosc2, "Falszywa")

Kasyno.set_start_state("Uczciwa")

wyniki = []
podmiany1 = []
podmiany2 = []

COUNT = 4000
true_diff = []
for i in range(COUNT):
    state, x, y, z = Kasyno.roll()
    wyniki.append(x)
    podmiany1.append(y)
    podmiany2.append(z)
    true_diff.append(state)
opt = viterbi_algorithm(wyniki, [0, 1], Switch_state, Emit_p)
opt2, F = Prefiks(wyniki, [0, 1], Switch_state, Emit_p)
B = sufiks(wyniki, [0, 1], Switch_state, Emit_p)

viterbi_percentage = np.sum([x == y for x, y in zip(true_diff, opt)]) / COUNT
pr_aposteriori = aposteriori(opt2, F, B)
pr_arg = np.argmax(pr_aposteriori, axis=-1)
aposteriori_percentage = np.sum([x == y for x, y in zip(true_diff, pr_arg)]) / COUNT

print(pr_arg)



#####                    PREZENTACJA SYMULACJI  ############################
result ={}
for i in range(1, 7):
    result[str(i)] = wyniki.count(i)



# Plot of zmiany czestotliwosci wystepowania 6 lel
ilosc_6 = 0
reszta = 0
proporcja_w_tym_czasie = []
iter = [x for x in range(COUNT)]

for i in range(len(wyniki)):
    if wyniki[i] == 6:
        ilosc_6 += 1
    else:
        reszta += 1

    proporcja_w_tym_czasie.append(ilosc_6/(reszta+ilosc_6))


a = list(result.keys())
b = list(result.values())

plt.subplot(2, 1, 1)
plt.title("Liczba poszczegolnych losowan.")
plt.bar(a, b)

plt.subplot(2, 1, 2)
plt.title("Ukazanie proporcji wystapien 6 do resty w stosunku do ilosci.")
plt.plot(iter[20::], proporcja_w_tym_czasie[20::])
plt.show()


cos = []
xx = []
for i in range(len(wyniki)):
    cos.append(wyniki[i])
    if len(cos) > 200:
        cos.pop(0)
    xx.append(cos.count(6) / len(cos))

plt.title("200 prob rotacyjnie.")
plt.semilogy(iter[20::], xx[20::])

wart =  np.multiply(podmiany1, xx)
plt.scatter(iter[20::], wart[20::], s=10 , c = "r")
wart =  np.multiply(podmiany2, xx)
plt.scatter(iter[20::], wart[20::], s=10, c = "g")
plt.show()
print(len(opt))
print(len(iter))

plt.title("Aposteriori, wynik: {0} %".format(aposteriori_percentage * 100))
plt.plot(iter, np.array(pr_arg), color="g", )
plt.scatter(iter, np.array(true_diff) , color="b", s=10)
plt.show()

plt.title("Viterbi, wynik: {0} %".format(viterbi_percentage * 100))
plt.plot(iter, np.array(opt) , color="r")
plt.scatter(iter, np.array(true_diff), color="b", s=10)
plt.show()

#####                    Koniec  prezentacji symulacji  ############################
