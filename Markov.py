import numpy as np
import numpy.random as random

random.seed(21)
class Gra:

    def __init__(self):

        self.number_of_states = 0
        self.Tranition_params = {}
        self.State_change = {}
        self.current_state = ""

    # Format of dict  = { "1" : 0.5, "2": 0.9}
    def set_dict_transition_for_state(self, slownik, name):

        try:
            self.Tranition_params[name] = slownik
            self.number_of_states = len(self.Tranition_params)
            return True
        except:
            return False

    # dict in format -- {  state1 :  {name : "prob1 od state1", name2:"prob2 of state2"  }      }
    def set_switch_prob_for_all_states(self, slownik):

        klucze = slownik.keys()
        wartosci = list(slownik.values())



        for i, k in enumerate(klucze):
            self.State_change[k] = wartosci[i]



    def set_start_state(self, name):
        self.current_state = name

    # ------Inner function-------
    def private_check_probability(self, slownik):
        for i in slownik.values():
            if i > 1 or i < 0:
                return False
        return True

    def private_losuj(self, probability):
        return random.random() < probability

    # Roll with current roll and get outcome
    def roll(self):

        get_trans_of_change_state= self.State_change[self.current_state]

        # Mozliwa_podmiana

        aktualna = np.copy(self.current_state)
        a = list(get_trans_of_change_state.keys())
        b= list(get_trans_of_change_state.values())
        self.current_state = np.random.choice(a, p=b)

        return int(self.current_state == "Falszywa"), float(np.random.choice(list(self.Tranition_params[self.current_state].keys()), p = list(self.Tranition_params[self.current_state].values()) ) ), \
               aktualna!=self.current_state == "Falszywa", aktualna != self.current_state == "Uczciwa"



class BioInf(Gra):

    def roll(self):

        return True